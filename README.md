WARNING: at this time node-sass-chokidar has security vulnerabilities because it has not updated it's dependencies.

## Notable features:

Full Redux CRUD example (not implemented yet)

Modern ReactJS + Bootstrap 4 + Flexbox.

SaSS support provided by node-sass-chokidar as recommended by the create-react-app project.

Handles singular case for blue info box.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Instructions

To run locally, clone the repo and run this script in the root directory:

./run.sh

To view on heroku:

https://johnwyatt-react2.herokuapp.com/
