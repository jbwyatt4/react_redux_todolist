import constants from '../constants'

export default {
  createTodoItem: () => {
    return {
      type: constants.CREATE_TODO_ITEM
    }
  },
  updateTodoItem: () => {
    return {
      type: constants.UPDATE_TODO_ITEM
    }
  },
  setEditingID: (id) => {
    return {
      type: constants.SET_EDITINGID,
      data: id
    }
  },
  clearEditingID: () => {
    return {
      type: constants.CLEAR_EDITINGID
    }
  }
}
