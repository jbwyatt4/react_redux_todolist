import React, { Component } from 'react'
import { connect } from 'react-redux'
//import PropTypes from 'prop-types'
import actions from '../actions/'
//import constants from '../constants'
//import Todo from './todo'
//import TodoForm from './todoForm'

class TodoListContainer extends Component {

  // pushs an empty object on to the list
  addTodo = () => {
    //console.log("AddTodo event called")
    this.props.createTodoItem();
    this.props.setEditingID(this.props.todos.length - 1);
  }

  selectTodo = (i,e) => {
    e.preventDefault();
    console.log("SelectTodo event called" + i);
    this.props.setEditingID(i);
  }

  genInfoBox = () => {
    let count = this.getActiveTodos();
    if(count > 1) {
      return (<div className="info_tasks_box">
              <h6>Complete all Tasks</h6>
              <p>You have {count} active tasks</p>
            </div>);
    // handle singular case
    } else if (count === 1) {
      return (<div className="info_tasks_box">
              <h6>Complete all Tasks</h6>
              <p>You have {count} active task</p>
            </div>);
    } else {
      return (<div className="info_completed_tasks_box">
              <h6>All Tasks Completed</h6>
              <p>Well done!</p>
            </div>);
    }
  }

  getActiveTodos() {
    let count = 0;
    this.props.todos.map((item, i) => {
      if (item.is_completed === false) {
        count = count + 1;
      }
    });
    return count;
  }

  render() {
    const list = this.props.todos;
    return (
    <div className="container todo_container">
      <h2>Your tasks</h2>

      {this.genInfoBox()}

      {console.log("List" + JSON.stringify(list))}
      {console.log("Editing ID " + JSON.stringify(this.props.editingTodoId))}

      <div className="todo_list row">
        {
          //TODO: move selectTodo call into it's own variable so react isn't recreating it with every render
          list.map((item, i) => {
            if(true) {
              return (
                <div className="col-sm-4 bottom-padding">
                <div className="todo" key={i} onClick={(e) => this.selectTodo(i, e)}>
                <div className="_bold">{item.name}</div>
                  <div
                    className={item.is_completed ? '_success' : '_standard'}>
                    {item.is_completed ? 'Completed' : 'Active'}</div>
                </div>
              </div>);
          } else {
            return (
              <div className="todoform" key={i}>
              </div>
            );
          }
          })
        }

        <div className="col-sm-4">
        <div className="todo" onClick={this.addTodo}>
          <div className="_center">+ Add Task</div>
          <div>

          </div>
        </div>
        </div>

      </div>
    </div>
  )
  }
}

const stateToProps = (state) => {
  return {
    todos: state.todos.todos,
    editingTodoId: state.editingTodoId.editingTodoId,
    activeTodos: state.activeTodos
  }
}

const dispatchToProps = (dispatch) => {
  return {
    createTodoItem: (todo) => dispatch(actions.createTodoItem()),
    setEditingID: (editingTodoId) => dispatch(actions.setEditingID(editingTodoId)),
  }
}

export default connect(stateToProps, dispatchToProps)( TodoListContainer)
