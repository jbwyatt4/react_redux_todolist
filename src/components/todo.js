import React, { Component } from 'react'

class Todo extends Component {
  render() {
  return (
    <div onClick={(e) => this.selectTodo(this.props.key, e)}>
      {this.props.item.name} {this.props.item.is_completed ? 'Completed' : 'Incomplete'}
    </div>
  )};
}

//export default connect(stateToProps, dispatchToProps)( TodoListContainer)
export default Todo;
