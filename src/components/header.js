import React from 'react'

const Header = () => (
  <div className="header">
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <a className="navbar-brand" href="">
        <div className="logo_styling">SimpleTask</div>
      </a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="collapsibleNavbar">
        <ul className="navbar-nav mr-auto">
        </ul>
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" href="">Hello, User!</a>
          </li>
        </ul>
      </div>
    </nav>
    <div className="decorative_line1">
    </div>
    <div className="decorative_line2">
    </div>
  </div>
)

export default Header
