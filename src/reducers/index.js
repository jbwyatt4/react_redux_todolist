import { combineReducers } from 'redux'
import todos from './todos'
import editingId from './editingId'

export default combineReducers({
  todos: todos, editingTodoId: editingId
})
