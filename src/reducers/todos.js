import constants from '../constants'

var initialState = {
  todos: [
    {name: "Groceries", is_completed: true},
    {name: "Laundry", is_completed: false}
  ],
  editingTodoId: null
}

const todos = (state = initialState, action) => {
  switch (action.type) {
    // pushs an empty object on to the list
    case constants.CREATE_TODO_ITEM:
      let newState = Object.assign({}, state);
      console.log("CREATE_TODO_ITEM: " + JSON.stringify(action));
      newState.todos.push({name: '', is_completed: false});
      return newState;
    default:
      return state;
  }
}

export default todos
