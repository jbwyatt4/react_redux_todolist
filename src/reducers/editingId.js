import constants from '../constants'

var initialState = {
  todos: [
    {name: "Groceries", is_completed: true},
    {name: "Laundry", is_completed: false}
  ],
  editingTodoId: null
}

const editingId = (state = initialState, action) => {
  let newState = Object.assign({}, state);
  switch (action.type) {
    case constants.SET_EDITINGID:
      newState.editingTodoId = action.data;
      return newState;
    case constants.CLEAR_EDITINGID:
      newState.editingTodoId = null;
      return newState;
    default:
      return state;
  }
}

export default editingId
