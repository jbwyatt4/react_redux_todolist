import React, { Component } from 'react';
import Header from './components/header'
import TodoListContainer from './components/todolistContainer'
import Footer from './components/footer'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <TodoListContainer />
        <Footer />
      </div>
    );
  }
}

export default App;
